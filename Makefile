# FLAGS
CCFLAGS 	:= -std=gnu11 -ffreestanding -O2 -Wall -Wextra

# FILES
KERNEL 		:= notos.bin
ISOKERNEL	:= notos.iso
LINKERFILE	:= linker.ld
OBJ			:= kernel.c.o boot.S.o

# DIRS
BUILDDIR	:= build	# TODO: Setup it!
ISODIR		:= iso

# COMMANDS
CC			:= i686-elf-gcc
ASSC		:= i686-elf-as

.PHONY: build
# Builds
build: $(KERNEL)
iso: $(ISOKERNEL)

# C
%.c.o: %.c
	$(CC) $(CCFLAGS) -c $< -o $@

# GNU Assembly
%.S.o: %.S
	$(ASSC) $< -o $@

# Kernel
$(KERNEL): $(OBJ)
	$(CC) -T $(LINKERFILE) -o $@ -ffreestanding -O2 -nostdlib $(OBJ) -lgcc

# Build ISO
$(ISOKERNEL): $(KERNEL)
	grub-file --is-x86-multiboot $(KERNEL)

	mkdir -p $(ISODIR)/boot/grub
	touch $(ISODIR)/boot/grub/grub.cfg
	echo 'menuentry "NotOS" {'				>> $(ISODIR)/boot/grub/grub.cfg
	echo '    multiboot /boot/$(KERNEL)'		>> $(ISODIR)/boot/grub/grub.cfg
	echo '}'								>> $(ISODIR)/boot/grub/grub.cfg
	
	cp $(KERNEL) $(ISODIR)/boot/$(KERNEL)
	grub-mkrescue -o $(ISOKERNEL) $(ISODIR)
	
run: $(ISOKERNEL)
	qemu-system-i386 -cdrom $(ISOKERNEL)

.PHONY: clean
clean:
	rm -rf $(OBJ) $(ISODIR) $(KERNEL)